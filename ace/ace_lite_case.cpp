//
// Created by yang_mingdi on 2022/1/22.
//
#include "js_async_work.h"
#include "message_queue_utils.h"
#include "ace_mem_base.h"
#include "memory_heap.h"
#include "jsi.h"
#include "jsi_types.h"
#include "gtest/gtest.h"
#include "../../../foundation/ace/ace_engine_lite/frameworks/native_engine/jsi/internal/jsi_internal.h"

using namespace std;
using namespace OHOS;
using namespace ACELite;
using namespace testing::ext;

void testfunction(){}
bool testfunction1_true(){ return true; }
bool testfunction1_false(){ return false; }


class ACELiteTest : public testing::Test
{
protected:
    static void SetUpTestCase(void)
    {
        printf("----------test case with ACELiteTest start-------------\n");
    }

    static void TearDownTestCase(void)
    {
        printf("----------test case with ACELiteTest end-------------\n");
    }
};

// 1
HWTEST_F(ACELiteTest, testAppQueueHandlerIsNotNullptr, Function | MediumTest | Level0)
{
    printf("------start testAppQueueHandlerIsNotNullptr------\n");
    bool ret;
    int value = 0;
    AbilityMsgId msgId = UNKNOWN;
    QueueHandler data{nullptr};
    JsAsyncWork::SetAppQueueHandler(&value);
    ret = JsAsyncWork::DispatchToLoop(msgId, data);
    EXPECT_EQ(ret, false);
    printf("------end testAppQueueHandlerIsNotNullptr------\n");
}
// 2
HWTEST_F(ACELiteTest, testAppQueueHandlerIsNullptr, Function | MediumTest | Level0)
{
    printf("------start testEnvInitialized------\n");
    bool ret;
    AbilityMsgId msgId = UNKNOWN;
    QueueHandler data{nullptr};
    JsAsyncWork::SetAppQueueHandler(nullptr);
    ret = JsAsyncWork::DispatchToLoop(msgId, data);
    EXPECT_EQ(ret, true);
    printf("------end testEnvInitialized------\n");
}
//3
HWTEST_F(ACELiteTest, testEnvInitializedIsTrue, Function | MediumTest | Level0)
{
  printf("------start testEnvInitializedIsTrue------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  JsAsyncWork::SetAppQueueHandler(nullptr);
  JsAsyncWork::SetEnvStatus(true);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, false);
  printf("------end testEnvInitializedIsTrue------\n");
}
//4
HWTEST_F(ACELiteTest, testEnvInitializedIsNotTrue, Function | MediumTest | Level0)
{
  printf("------start testEnvInitializedIsNotTrue------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  JsAsyncWork::SetAppQueueHandler(nullptr);
  JsAsyncWork::SetEnvStatus(false);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, true);
  printf("------end testEnvInitializedIsNotTrue------\n");
}
//5
HWTEST_F(ACELiteTest, testMsgIdIsGe, Function | MediumTest | Level0)
{
  printf("------start testMsgIdIsGe------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, true);
  printf("------end testMsgIdIsGe------\n");
}
//6
HWTEST_F(ACELiteTest, testIsFatalErrorHittedIsNotNullptrAndTrue, Function | MediumTest | Level0)
{
  printf("------start testIsFatalErrorHittedIsNotNullptrAndTrue------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted = testfunction1_true;
  FatalHandleFunc isAppExiting = testfunction1_true;
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, true);
  printf("------end testIsFatalErrorHittedIsNotNullptrAndTrue------\n");
}
//7
HWTEST_F(ACELiteTest, testIsFatalErrorHittedIsNotNullptrAndFalse, Function | MediumTest | Level0)
{
  printf("------start testIsFatalErrorHittedIsNotNullptrAndFalse------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted = testfunction1_false;
  FatalHandleFunc isAppExiting = testfunction1_true;
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, false);
  printf("------end testIsFatalErrorHittedIsNotNullptrAndFalse------\n");
}
//8
HWTEST_F(ACELiteTest, testIsFatalErrorHittedIsNullptr, Function | MediumTest | Level0)
{
  printf("------start testIsFatalErrorHittedIsNullptr------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted {nullptr};
  FatalHandleFunc isAppExiting = testfunction1_true;
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, false);
  printf("------end testIsFatalErrorHittedIsNullptr------\n");
}
//9
HWTEST_F(ACELiteTest, testIsAppExitingIsNotNullptrAndTrue, Function | MediumTest | Level0)
{
  printf("------start testIsAppExitingIsNotNullptrAndTrue------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted = testfunction1_true;
  FatalHandleFunc isAppExiting = testfunction1_true;
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, true);
  printf("------end testIsAppExitingIsNotNullptrAndTrue------\n");
}
//10
HWTEST_F(ACELiteTest, testIsAppExitingIsNotNullptrAndFalse, Function | MediumTest | Level0)
{
  printf("------start testIsAppExitingIsNotNullptrAndFalse------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted = testfunction1_true;
  FatalHandleFunc isAppExiting = testfunction1_false;
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, false);
  printf("------end testIsAppExitingIsNotNullptrAndFasle------\n");
}
//11
HWTEST_F(ACELiteTest, testIsAppExitingIsNullptr, Function | MediumTest | Level0)
{
  printf("------start testIsAppExitingIsNullptr------\n");
  bool ret;
  AbilityMsgId msgId = UNKNOWN;
  QueueHandler data{nullptr};
  FatalHandleFunc isFatalErrorHitted = testfunction1_true;
  FatalHandleFunc isAppExiting = {nullptr};
  JsAsyncWork::SetFatalHandleFunc(isFatalErrorHitted, isAppExiting);
  ret = JsAsyncWork::DispatchToLoop(msgId, data);
  EXPECT_EQ(ret, false);
  printf("------end testIsAppExitingIsNullptr------\n");
}
//12
HWTEST_F(ACELiteTest, testPutMessagehandlerIsNotNullptr, Function | MediumTest | Level0)
{
  printf("------start testPutMessagehandlerIsNotNullptr------\n");
  int8_t ret;
  ret = MessageQueueUtils::PutMessage(testfunction,testfunction,0);
  EXPECT_EQ(ret, 0);
  printf("------end testPutMessagehandlerIsNotNullptr------\n");
}
//13
HWTEST_F(ACELiteTest, testPutMessagehandlerIsNullptr, Function | MediumTest | Level0)
{
  printf("------start testPutMessagehandlerIsNullptr------\n");
  int8_t ret;
  ret = MessageQueueUtils::PutMessage(nullptr,testfunction,0);
  EXPECT_EQ(ret, -1);
  printf("------end testPutMessagehandlerIsNullptr------\n");
}
//14
HWTEST_F(ACELiteTest, testPutMessagemsgPtrIsNotNullptr, Function | MediumTest | Level0)
{
  printf("------start testPutMessagemsgPtrIsNotNullptr------\n");
  int8_t ret;
  ret = MessageQueueUtils::PutMessage(testfunction,testfunction,0);
  EXPECT_EQ(ret, 0);
  printf("------end testPutMessagemsgPtrIsNotNullptr------\n");
}
//15
HWTEST_F(ACELiteTest, testPutMessagemsgPtrIsNullptr, Function | MediumTest | Level0)
{
  printf("------start testPutMessagemsgPtrIsNullptr------\n");
  int8_t ret;
  ret = MessageQueueUtils::PutMessage(testfunction, nullptr,0);
  EXPECT_EQ(ret, -1);
  printf("------end testPutMessagemsgPtrIsNullptr------\n");
}
//16
HWTEST_F(ACELiteTest, testCreateMessageQueue, Function | MediumTest | Level0)
{
  printf("------start testCreateMessageQueue------\n");
  QueueHandler ret;
  ret = MessageQueueUtils::CreateMessageQueue(0,0);
  EXPECT_EQ(ret, nullptr);
  printf("------end testCreateMessageQueue------\n");
}
//17
HWTEST_F(ACELiteTest, testDeleteMessageQueue, Function | MediumTest | Level0)
{
  printf("------start testCreateMessageQueue------\n");
  int8_t ret;
  ret = MessageQueueUtils::DeleteMessageQueue(nullptr);
  EXPECT_EQ(ret, -1);
  printf("------end testCreateMessageQueue------\n");
}
//18
HWTEST_F(ACELiteTest, testGetMessage, Function | MediumTest | Level0)
{
  printf("------start testGetMessage------\n");
  int8_t ret;
  ret = MessageQueueUtils::GetMessage(nullptr, nullptr,0);
  EXPECT_EQ(ret, -1);
  printf("------end testGetMessage------\n");
}

//19
HWTEST_F(ACELiteTest, testGetGlobalObjectRight, Function | MediumTest | Level0)
{
    printf("------start testGetGlobalObjectRight------\n");
    auto rightRet = AS_JSI_VALUE(jerry_get_global_object());
    auto jsi = JSI::GetGlobalObject();
    EXPECT_EQ(jsi,rightRet);
    printf("------end testGetGlobalObjectRight------\n");
}

//20
HWTEST_F(ACELiteTest, testGetGlobalObjectError, Function | MediumTest | Level0)
{
    printf("------start testGetGlobalObjectError------\n");
    auto rightRet = AS_JSI_VALUE(jerry_get_global_object());
    auto jsi = JSI::GetGlobalObject();
    EXPECT_NE(jsi,rightRet);
    printf("------end testGetGlobalObjectError------\n");
}

//21
HWTEST_F(ACELiteTest, testCreateObjectRight, Function | MediumTest | Level0)
{
    printf("------start testCreateObjectRight------\n");
    auto rightRet = AS_JSI_VALUE(jerry_create_object());
    auto jsi = JSI::CreateObject();
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateObjectRight------\n");
}

//22
HWTEST_F(ACELiteTest, testCreateObjectError, Function | MediumTest | Level0)
{
    printf("------start testCreateObjectError------\n");
    auto rightRet = AS_JSI_VALUE(jerry_create_object());
    auto jsi = JSI::CreateObject();
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateObjectError------\n");
}

//23
HWTEST_F(ACELiteTest, testCreateFunctionRight, Function | MediumTest | Level0)
{
    printf("------start testCreateFunctionRight------\n");
    JSIFunctionHandler handler = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateFunction(handler);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateFunctionRight------\n");
}

//24
HWTEST_F(ACELiteTest, testCreateFunctionError, Function | MediumTest | Level0)
{
    printf("------start testCreateFunctionError------\n");
    JSIFunctionHandler handler = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateFunction(handler);
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateFunctionError------\n");
}

//25
HWTEST_F(ACELiteTest, testCreateStringNullRight, Function | MediumTest | Level0)
{
    printf("------start testCreateStringNullRight------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateString(str);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateStringNullRight------\n");
}

//26
HWTEST_F(ACELiteTest, testCreateStringNullError, Function | MediumTest | Level0)
{
    printf("------start testCreateStringNullError------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateString(str);
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateStringNullError------\n");
}

//27
HWTEST_F(ACELiteTest, testCreateStringNotNullRight, Function | MediumTest | Level0)
{
    printf("------start testCreateStringNotNullRight------\n");
    char str[] = "hello world!";
    auto rightRet = AS_JSI_VALUE(jerry_create_string(reinterpret_cast<const jerry_char_t *>(str)));
    auto jsi = JSI::CreateString(str);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateStringNotNullRight------\n");
}

//28
HWTEST_F(ACELiteTest, testCreateStringNotNullError, Function | MediumTest | Level0)
{
    printf("------start testCreateStringNotNullError------\n");
    char str[] = "hello world!";
    auto rightRet = AS_JSI_VALUE(jerry_create_string(reinterpret_cast<const jerry_char_t *>(str)));
    auto jsi = JSI::CreateString(str);
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateStringNotNullError------\n");
}

//29
HWTEST_F(ACELiteTest, testCreateStringWithBufferSizeNullRight, Function | MediumTest | Level0)
{
    printf("------start testCreateStringWithBufferSizeNullRight------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateStringWithBufferSize(str, 5);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateStringWithBufferSizeNullRight------\n");
}

//30
HWTEST_F(ACELiteTest, testCreateStringWithBufferSizeNullError, Function | MediumTest | Level0)
{
    printf("------start testCreateStringWithBufferSizeNullError------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::CreateStringWithBufferSize(str, 5);
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateStringWithBufferSizeNullError------\n");
}

//31
HWTEST_F(ACELiteTest, testCreateStringWithBufferSizeNotNullRight, Function | MediumTest | Level0)
{
    printf("------start testCreateStringWithBufferSizeNotNullRight------\n");
    char str[] = "hello world!";
    auto rightRet = AS_JSI_VALUE(jerry_create_string_sz(reinterpret_cast<const jerry_char_t *>(str), size));
    auto jsi = JSI::CreateStringWithBufferSize(str, sizeof(str));
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateStringWithBufferSizeNotNullRight------\n");
}

//32
HWTEST_F(ACELiteTest, testCreateStringWithBufferSizeNotNullError, Function | MediumTest | Level0)
{
    printf("------start testCreateStringWithBufferSizeNotNullError------\n");
    char str[] = "hello world!";
    auto rightRet = AS_JSI_VALUE(jerry_create_string_sz(reinterpret_cast<const jerry_char_t *>(str), size));
    auto jsi = JSI::CreateStringWithBufferSize(str, sizeof(str));
    EXPECT_NE(jsi,rightRet);
    printf("------end testCreateStringWithBufferSizeNotNullError------\n");
}

//33
HWTEST_F(ACELiteTest, testCreateUndefinedRight, Function | MediumTest | Level0)
{
    printf("------start testCreateUndefinedRight------\n");
    auto rightRet = AS_JSI_VALUE(jerry_create_undefined());
    auto jsi = JSI::CreateUndefined();
    EXPECT_EQ(jsi,rightRet);
    printf("------end testCreateUndefinedRight------\n");
}

//34
HWTEST_F(ACELiteTest, testtestCreateUndefinedError, Function | MediumTest | Level0)
{
    printf("------start testtestCreateUndefinedError------\n");
    auto rightRet = AS_JSI_VALUE(jerry_create_undefined());
    auto jsi = JSI::CreateUndefined();
    EXPECT_NE(jsi,rightRet);
    printf("------end testtestCreateUndefinedError------\n");
}

//35
HWTEST_F(ACELiteTest, testValueIsFunctionRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsFunctionRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_function(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsFunction(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsFunctionRight------\n");
}

//36
HWTEST_F(ACELiteTest, testValueIsFunctionError, Function | MediumTest | Level0)
{
    printf("------start testValueIsFunctionError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_function(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsFunction(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsFunctionError------\n");
}

//37
HWTEST_F(ACELiteTest, testValueIsUndefinedRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsUndefinedRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_undefined(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsUndefined(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsUndefinedRight------\n");
}

//38
HWTEST_F(ACELiteTest, testValueIsUndefinedError, Function | MediumTest | Level0)
{
    printf("------start testValueIsUndefinedError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_undefined(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsUndefined(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsUndefinedError------\n");
}

//39
HWTEST_F(ACELiteTest, testValueIsNumberRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsNumberRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_number(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsNumber(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsNumberRight------\n");
}

//40
HWTEST_F(ACELiteTest, testValueIsNumberError, Function | MediumTest | Level0)
{
    printf("------start testValueIsNumberError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_number(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsNumber(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsNumberError------\n");
}

//41
HWTEST_F(ACELiteTest, testValueIsStringRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsStringRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_string(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsString(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsStringRight------\n");
}

//42
HWTEST_F(ACELiteTest, testValueIsStringError, Function | MediumTest | Level0)
{
    printf("------start testValueIsStringError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_string(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsString(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsStringError------\n");
}

//43
HWTEST_F(ACELiteTest, testValueIsBooleanRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsBooleanRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_boolean(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsBoolean(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsBooleanRight------\n");
}

//44
HWTEST_F(ACELiteTest, testValueIsBooleanError, Function | MediumTest | Level0)
{
    printf("------start testValueIsBooleanError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_boolean(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsBoolean(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsBooleanError------\n");
}

//45
HWTEST_F(ACELiteTest, testValueIsNullRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsNullRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_null(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsNull(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsNullRight------\n");
}

//46
HWTEST_F(ACELiteTest, testValueIsNullError, Function | MediumTest | Level0)
{
    printf("------start testValueIsNullError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_null(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsNull(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsNullError------\n");
}

//47
HWTEST_F(ACELiteTest, testValueIsObjectRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsObjectRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_object(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsObject(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsObjectRight------\n");
}

//48
HWTEST_F(ACELiteTest, testValueIsObjectError, Function | MediumTest | Level0)
{
    printf("------start testValueIsObjectError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_object(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsObject(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsObjectError------\n");
}

//49
HWTEST_F(ACELiteTest, testValueIsErrorRight, Function | MediumTest | Level0)
{
    printf("------start testValueIsErrorRight------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_error(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsError(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testValueIsErrorRight------\n");
}

//50
HWTEST_F(ACELiteTest, testValueIsErrorError, Function | MediumTest | Level0)
{
    printf("------start testValueIsErrorError------\n");
    JSIValue value;
    auto rightRet = jerry_value_is_error(AS_JERRY_VALUE(value));
    auto jsi = JSI::ValueIsError(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testValueIsErrorError------\n");
}

//51
HWTEST_F(ACELiteTest, testJsonStringifyRight, Function | MediumTest | Level0)
{
    printf("------start testJsonStringifyRight------\n");
    JSIValue value;
    auto rightRet = nullptr;
    auto jsi = JSI::JsonStringify(value);
    EXPECT_NE(jsi,rightRet);
    printf("------end testJsonStringifyRight------\n");
}

//52
HWTEST_F(ACELiteTest, testJsonStringifyError, Function | MediumTest | Level0)
{
    printf("------start testJsonStringifyError------\n");
    JSIValue value;
    auto rightRet = nullptr;
    auto jsi = JSI::JsonStringify(value);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testJsonStringifyError------\n");
}

//53
HWTEST_F(ACELiteTest, testJsonParseNullRight, Function | MediumTest | Level0)
{
    printf("------start testJsonParseNullRight------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::JsonParse(str);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testJsonParseNullRight------\n");
}

//54
HWTEST_F(ACELiteTest, testJsonParseNullError, Function | MediumTest | Level0)
{
    printf("------start testJsonParseNullError------\n");
    char* str = nullptr;
    auto rightRet = JSI::CreateUndefined();
    auto jsi = JSI::JsonParse(str);
    EXPECT_NE(jsi,rightRet);
    printf("------end testJsonParseNullError------\n");
}

//55
HWTEST_F(ACELiteTest, testJsonParseNotNullRight, Function | MediumTest | Level0)
{
    printf("------start testJsonParseNotNullRight------\n");
    char str[] = "hello world!";
    auto rightRet = jerry_json_parse(reinterpret_cast<const jerry_char_t *>(str), strlen(str));
    auto jsi = JSI::JsonParse(str);
    EXPECT_EQ(jsi,rightRet);
    printf("------end testJsonParseNotNullRight------\n");
}

//56
HWTEST_F(ACELiteTest, testJsonParseNotNullError, Function | MediumTest | Level0)
{
    printf("------start testJsonParseNotNullError------\n");
    char str[] = "hello world!";
    auto rightRet = jerry_json_parse(reinterpret_cast<const jerry_char_t *>(str), strlen(str));
    auto jsi = JSI::JsonParse(str);
    EXPECT_NE(jsi,rightRet);
    printf("------end testJsonParseNotNullError------\n");
}
